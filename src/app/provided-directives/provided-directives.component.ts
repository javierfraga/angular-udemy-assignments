import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-provided-directives',
  templateUrl: './provided-directives.component.html',
  styleUrls: ['./provided-directives.component.css'],
  styles: [`
  	.blue {
  		background-color:blue;
  		color:white;
  	}
  `],  
  encapsulation: ViewEncapsulation.None
})
export class ProvidedDirectivesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  displayText:boolean = false;
  clicks:Array<string> = [];

  recordClicks() {
  	this.displayText = !this.displayText;
  	this.clicks.push(Date())
  }

}
