import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ProvidedDirectivesComponent } from './provided-directives/provided-directives.component';


@NgModule({
  declarations: [
    AppComponent,
    ProvidedDirectivesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
