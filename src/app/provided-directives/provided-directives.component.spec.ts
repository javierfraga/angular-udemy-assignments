import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidedDirectivesComponent } from './provided-directives.component';

describe('ProvidedDirectivesComponent', () => {
  let component: ProvidedDirectivesComponent;
  let fixture: ComponentFixture<ProvidedDirectivesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidedDirectivesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvidedDirectivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
